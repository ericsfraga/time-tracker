;;; time-tracker.el --- Track time in emacs -*- lexical-binding: t -*-

;; Copyright (C) 2021  Free Software Foundation, Inc.

;; Author: Eric S Fraga <e.fraga@ucl.ac.uk>
;; Maintainer: Eric S Fraga <e.fraga@ucl.ac.uk>
;; URL: 
;; Version: 0.0.1
;; Package-Requires: ((emacs "27.1") )
;; Keywords: tools

;; Acknowledgements: this code was inspired by the whackatime package
;; written by Mark Dawson <markgdawson@gmail.com>

;; This file is part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Take a snapshot every so often, recording the current buffer and
;; time unless idle in which case the total idle time is recorded.

;;; Code:

(defvar time-tracker-log-filename (concat user-emacs-directory
                                          "time-tracker-log-"
                                          (replace-regexp-in-string
                                           "\n" ""
                                           (shell-command-to-string "hostname"))
                                          ".org")
  "File in which to log time-tracker entries.")

(defun time-tracker-start ()
  "Start tracking time log."
  (interactive)
  (time-tracker--start-timer))

(defun time-tracker-stop ()
  "Stop tracking activity."
  (interactive)
  (time-tracker--stop-timer))

(defun time-tracker--file-or-buffer-name (buff)
  "Return buffer name for buffer BUFF."
  (cond ((and
          (buffer-file-name buff)
          (not (auto-save-file-name-p (buffer-file-name buff))))
         (buffer-file-name buff))
        ;; org edit src buffer
        ((bound-and-true-p org-src--beg-marker)
         (buffer-file-name (marker-buffer org-src--beg-marker)))
        ((string= major-mode 'dired-mode) default-directory)
        ((buffer-name buff))))

(defun time-tracker--log-message (message)
  "Log the activity with MESSAGE."
  (let ((inhibit-message t)
        (message-log-max nil))
    (append-to-file message nil time-tracker-log-filename)))

;; Idle timer and notifications

(defvar time-tracker--timer nil
  "The timer for tracking time.")

(defvar time-tracker-tick 60
  "Number of seconds between ticks.")

(defvar time-tracker-idle-time 600
  "Number of seconds to classify as idle.")

(defun time-tracker--log-idle ()
  "Log an entry indicating Emacs is idle."
  (time-tracker--log-message
   (format "| %s | %d | nil | idle |\n"
           ;; (time-convert (current-time) 'integer)
           (format-time-string (org-time-stamp-format t t))
           ;; (org-timestamp-from-time (current-time) t t)
           (time-tracker--idle-seconds))))

(defun time-tracker--log-active (buffer)
  "Log an entry showing which buffer is currently active."
  (let ((msg
         (format "| %s | %d | %s | =%s= |\n"
                 ;; (time-convert (current-time) 'integer)
                 (format-time-string (org-time-stamp-format t t))
                 ;; (org-timestamp-from-time (current-time) t t)
                 (time-tracker--idle-seconds)
                 major-mode
                 ;; protect the table from spurious column dividers
                 (string-replace "|" "-" (time-tracker--file-or-buffer-name buffer)))))
    ;; (message msg)
    (time-tracker--log-message msg)))

(defun time-tracker--idle-seconds ()
  "Return the number of seconds for which Emacs has been idle."
  (if-let (idle-lisp (current-idle-time))
      (time-convert idle-lisp 'integer)
    0))

(defun time-tracker--idle-p ()
  "Check if number of idle seconds is greater than idle threshold."
  (> (time-tracker--idle-seconds) time-tracker-idle-time))

(defun time-tracker--record-tick ()
  "Run every TIME-TRACKER-TICK seconds to record current activity."
  (when (not (time-tracker--idle-p))
    (time-tracker--log-active (current-buffer))))

(defun time-tracker--stop-timer ()
  "Stop the timer for checking idle status."
  (when (timerp time-tracker--timer)
    (setq time-tracker--timer (cancel-timer time-tracker--timer))))

(defun time-tracker--start-timer ()
  "Start the timer for recording time log."
  (time-tracker--stop-timer)
  ;; (time-tracker--log-message "\n| time | idle | mode | buffer/file |\n|-\n")
  (setq time-tracker--timer
        (run-at-time 0 time-tracker-tick #'time-tracker--record-tick)))

(provide 'time-tracker)
;;; time-tracker.el ends here
